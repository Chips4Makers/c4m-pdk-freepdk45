# SPDX-License-Identifier: GPL-2.0-or-later OR AGPL-3.0-or-later OR CERN-OHL-S-2.0+
import os
from textwrap import dedent

try:
    import pdkmaster
except:
    print("Failed to load pdkmaster")
    exit(20)


with open(os.environ["PDKMASTER_MAKEFILE"], "w") as f:
    f.write(dedent(f"""
        # Autogenerated file
        # SPDX-License-Identifier: GPL-2.0-or-later OR AGPL-3.0-or-later OR CERN-OHL-S-2.0+
        PDKMASTER_PATH := {pdkmaster.__path__[0]}
    """[1:]))
