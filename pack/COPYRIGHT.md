Git is used to track authorship of the code of the C4M FreePDK45 PDK.  
An overview of the authors and their commits can be found in [shortlog.txt](shortlog.txt). For a full history of the project and code authorship head over the [gitlab repo](https://gitlab.com/Chips4Makers/c4m-pdk-freepdk45)
